"""@file HW0x02.py
@brief Two story elevator control
@details Shows the position of a two story elevator system as a result of random button pressing 

Source code can be found at https://bitbucket.org/dfreem03/me405_labs/src/master/HW0x02/

@author: Daniel Freeman
@date 1/25/21
"""

import random
import time

# Function definitions go here
def motor_cmd(cmd): 
    ''' @brief Tells motor how to move
        @details This function tells the elevator motor whether to move up, down or stop
        @param cmd desired command for the motor
        @return Function returns either a state change or staying on the current state
    '''
    if cmd== 'UP':
        print('Motor up')
    if cmd== 'DOWN':
        print('Motor down')
    if cmd== 'STOP':
        print('Motor stop')
        
def button_1():
    '''@brief First floor button
       @details This function controls the pushing on the first floor button
       @return Returns true (pressed) or false (not pressed)
    '''
    return random.choice([True, False]) # Randomly returns T or F


def button_2():
    '''@brief Second floor button
       @details This function controls the pushing on the first floor button
       @return Returns true (pressed) or false (not pressed)
    '''
    return random.choice([True, False]) # Randomly returns T or F


def floor_1():
    '''@brief First floor indicator
       @details Shows whether the elevator is on the first floor 
       @return Returns true (on first floor) or false (not on first floor)
    '''
    return random.choice([True, False]) # Randomly returns T or F


def floor_2():
    '''@brief Second floor indicator
       @details Shows whether the elevator is on the second floor 
       @return Returns true (on second floor) or false (not on second floor)
    '''
    return random.choice([True, False]) # Randomly returns T or F


# Main program
# This code only runs if the script is executed as main by pressing play 
# but does not run if the script is imported as a module

if __name__ == "__main__":
    # Program initialization goes here
    ## Elevator state
    # @brief Shows the state of the elevator
    # @details State 0 = Moving down
    # State 1 = Stopped on first floor
    # State 2 = Moving up
    # State 3 = Stopped on second floor
    
    state = 0 # Initial state
    
    while True:
        try:
            # Main program
            if state == 0:
                # Run state 0 first
                print('S0')
                if floor_1():
                    motor_cmd('DOWN') # Move down 
                    button_1() == False
                    state = 1 # Move to next state
            
            elif state == 1:
                # Run state 1
                print('S1')
                if button_1():
                    button_1() == False # You are already on first floor
                elif button_2():
                    motor_cmd('UP') # Move up to first floor
                    state = 2 # Move to next state
                
            elif state == 2:
                # Run state 2
                print('S2')
                if floor_2(): # If button 1 is pressed 
                    motor_cmd('STOP') # Stop on 2nd floor
                    button_2() == False
                    state = 3 # Move to next state
                
            elif state == 3:
                # Run state 3
                print('S3')
                if button_2(): # If button 2 is pressed 
                    button_2() == False
                elif button_1():
                    motor_cmd('DOWN')
                    state = 0 # Move to next state
                    
            else:
                pass
            
            time.sleep(0.2)
            
        except KeyboardInterrupt:
            break
                    
          
