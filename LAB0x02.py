'''@file LAB0x02.py
@brief Changes LED waveform as a result of button press
@details Uses callback function to detect falling edge on C13 user button on nucleo. When falling edge detected,
flag variable set to 1 and state is change to either square, sine or saw wave  

Source code can be found at https://bitbucket.org/dfreem03/me405_labs/src/master/LAB0x02.py

@image html IMG-9602.jpg
@author: Daniel
@date 1/28/2021
'''

import pyb
import utime
import math

def onButtonPressFCN(IRQ_src):
    '''@brief Callback function
       @details Changes flag variable from zero to one as a result of an interrupt caused by a falling voltage on pin C13
       @param IRQ_src Voltage on pin C13
       @returns Changes flag variable from zero to one
    '''
    ## Callback variable
    # @brief variable connected to callback function
    # @details If button is pressed, flag changes from 0 to 1
    global flag
    flag = 1  

if __name__ == "__main__":

    ## Button pin
    #@brief pinC13 is the blue user button on the nucleo
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    ## LED pin
    #@brief pinA5 is the LED on the nucleo
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    ## LED state
    # @brief Shows the state of the LED
    # @details State 0 = LED off, welcome state
    # State 1 = Square wave
    # State 2 = Sine wave
    # State 3 = Saw wave
    state = 0 # Initial state
    ## Callback variable
    # @brief variable connected to callback function
    # @details If button is pressed, flag changes from 0 to 1
    flag = 0 # Initial flag variable set to zero
    ## IRQ_FALLING
    #@brief Detects falling voltage on pin C13
    IRQ_FALLING = 0
    ## PULL_NONE
    # Does not activate any pull up or pull down resistors
    PULL_NONE = 0
    ## callback
    #@brief Gives the function to call when button is pushed
    callback = 0
    ## mode
    mode = 0
    ## pull
    #@brief Tell which resisitors to pull
    pull = 0
    ## Interrupt
    #@brief Program interrupt
    #@details Detects a falling edge on pinC13 and triggers the callback function
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN) # Button interrupt to call callback function
    ## Timer
    #@brief Creates a timer object using timer 2, triggers at 20kHz
    tim2 = pyb.Timer(2, freq = 20000)
    ## Channel configure
    #@brief Configures channel 1 to toggle pinA5 using PWM from tim2
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    while True:
        ## Time
        #@ Shows time since start of program in ms
        time = utime.ticks_ms() # Time since program start in ms
        try:
            # Main program
            if state == 0:
                ## Local time
                #@ Shows the time since the start of the if statement
                start = utime.ticks_ms() # Starts local clock
                print('Welcome, please press the blue button to cycle between square, sine and saw waves')
                while flag == 0:
                    pass
                flag = 1 # Set flag variable
                state = 1 # Move to next state
                print('Square wave selected')
                flag = 0 # Reset flag variable
                start = utime.ticks_ms()
 
            elif state == 1: # Run state 1
                ## t variable
                #@brief Waveform time
                #@details Finds the difference between the time since the start of the program and the time since the start of the if statement
                t = utime.ticks_diff(time, start)/1000
                ## Square
                #@brief Finds when the modulus of the value of t is greater than 0.5
                #@returns True or false
                square = 100*(t % 1 < 0.5) 
                t2ch1.pulse_width_percent(square)
                if flag == 1:
                    state = 2 # Move to next state
                    print('Sine wave selected')
                    flag = 0
                    start = utime.ticks_ms()
                
            elif state == 2:  # Run state 2
                t = utime.ticks_diff(time, start)/1000
                ## Sine
                #@brief Converts t into a sine wave of the appropriate amplitude, wavelength and offset
                sine = 0.5*(math.sin((math.pi*0.2)*t))+0.5
                t2ch1.pulse_width_percent(100*sine)
                if flag == 1:
                    state = 3 # Move to next state
                    print('Saw wave selected')
                    flag = 0
                    start = utime.ticks_ms()
                
            elif state == 3: # Run state 3
                t = utime.ticks_diff(time, start)/1000
                ## Saw
                #@brief Sets the duty cycle to be the modulus of the value of t 
                saw = 100*(t % 1)
                t2ch1.pulse_width_percent(saw)
                if flag == 1:
                    state = 1 # Move to next state
                    print('Square wave selected')
                    flag = 0
            
        except KeyboardInterrupt:
            break
    pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=None)
                    
          

                    

