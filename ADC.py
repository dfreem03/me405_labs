"""@file ADC.py
@brief Module to measure internal temperature of microcontroller
@details Module uses the pyb.ADCA11 class to measure the internal temperature of the microcontroller
@author: Daniel Freeman
"""
import pyb
## Sets ADCAll to have 12 bit resolution, with all analog inputs active
adcall = pyb.ADCAll(12, 0xFFFF)

class internal:
    def internal(self):
        '''@brief This method gets the internal temperature of the microcontroller in degrees celsius
        @returns stm_temp, the temperature in degrees celsius
        '''
        adcall.read_vref()
        self.stm_temp = adcall.read_core_temp()
        return self.stm_temp
