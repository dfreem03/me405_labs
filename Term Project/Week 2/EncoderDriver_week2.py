"""@file EncoderDriver_week2.py
@brief This driver operates the encoders on the motors
@details The driver works by first creating encoder object using hardware timers,
and then using methods to update and send the encoder position to EncoderTask
@author Daniel Freeman    
"""
import pyb
import utime

class EncoderDriver:
    '''@brief Class to set up encoders
    @details This class sets up encoders using hardware times. 
    '''
    def __init__(self):
        '''@brief Constructor for EncoderDriver
           @details Initializes pins and timers needed for both encoders to operate
        '''
        ## The current state of the finite state machine
        self.state = 0
        ## Pin B6
        self.pinPB6 = pyb.Pin(pyb.Pin.cpu.B6)
        ## Pin B7
        self.pinPB7 = pyb.Pin(pyb.Pin.cpu.B7) 
        ## The period of the encoder, largest 16-bit number
        self.period = 65535
        ## Timer for encoder 1, using timer 4
        self.timer = pyb.Timer(4, prescaler = 0, period = self.period)
        ## Encoder 1 channel 1
        self.t4ch1 = self.timer.channel(1, pyb.Timer.ENC_AB, pin = self.pinPB6)
        ## Encoder 1 channel 2
        self.t4ch2 = self.timer.channel(2, pyb.Timer.ENC_AB, pin = self.pinPB7)
        ## The initial position of the encoder
        self.position = 0
        ## The start time of the program
        self.thisTime = utime.ticks_ms()
        ## The initial delta of the encoder
        self.delta = 0
        ## The inital encoder count
        self.count = self.timer.counter()
        print ('Creating an encoder driver')
          
        # ONE REVOLUTION IS 4000 TICKS
        
    def run(self):
        '''@brief Runs the encoder driver
        '''
        ## Timestamp for start of run method
        self.start = utime.ticks_ms()
        print('Encoder Driver running')
        if utime.ticks_diff(self.start, self.thisTime) >= 100:
            self.update
            self.thisTime = utime.ticks_ms()

    def update(self):
        '''@brief Updates position
        '''
        print('Update method running')
        self.delta = self.timer.counter() - self.count
        if self.delta > -0.5*self.period and self.delta < 0.5*self.period:
            pass
        elif self.delta > 0.5*self.period:
            self.delta = self.delta - self.period
        elif self.delta < 0.5*self.period:
            self.delta = self.delta + self.period
        pass
        self.position += self.delta 
        self.count = self.timer.counter()
         
    def get_position(self):
        ''' @brief Gets most recent position of encoder
        '''
        return self.position
        
    def set_position(self, pos):
        ''' @brief Sets encoder value to specified value
        '''
        self.position = pos
            
    def get_delta(self):
        ''' @brief Gets most recent delta of encoder
            @return Returns delta in ticks
        '''
        return self.delta
        
        
    
        