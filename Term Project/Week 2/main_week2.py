# -*- coding: utf-8 -*-
"""@file main_week2.py
@brief This file tells the encoder what to do based on serial coms
@details This file acts as the handshake between the UI2.py and EncoderDriver_week2.py.
It receives serial input and tells the EncoderDriver what to do
@author: Daniel
"""
import pyb
from pyb import UART
import EncoderDriver_week2

## Serial port
myuart = UART(2)
## Current state of FSM
state = 0
## Create an object of the class EncoderDriver
EncoderDriver = EncoderDriver_week2.EncoderDriver()
while True:
    if state == 0:
        if myuart.any() !=0:
            ## Incoming character ASCII value
            val = myuart.readchar()
            if val == 122: # If character is a z
                state = 1
            elif val == 112: # If character is a p
                state = 2
            elif val == 100: # If character is a d
                state = 3
            elif val == 103: # If character is a g
                state = 4
            elif val == 115: # If character is an s
                state = 5
                
    elif state == 1:
        EncoderDriver.set_position(0) # Zero the encoder position
        state = 0
    
    elif state == 2:
        print('Encoder Position is ' + str(EncoderDriver.get_position)) # Print encoder position
        myuart.write(str(EncoderDriver.get_position)) # Send position over serial as a string
        state = 0
        
    elif state == 3:
        print('Encoder Delta is ' + str(EncoderDriver.get_delta)) # Print encoder delta
        myuart.write(str(EncoderDriver.get_delta)) # Send delta over serial as a string
        state = 0
        
    elif state == 4:
        EncoderDriver.run() # Start data collection
        state = 0
        
    elif state == 5:
        state = 0
        
    