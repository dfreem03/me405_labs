# -*- coding: utf-8 -*-
"""@file UI_week2.py
 @brief User Interface to interface with encoders
 @details Asks the user to press keys corresponding to several different encoder operations

@author: Daniel
"""
import serial

## Serial port
ser = serial.Serial(port='COM4',baudrate=115273,timeout=1)
## Current state of the finite state machine
state = 0

while True:
    if state == 0:
        ## Input character
        inv = input('Press z to zero the encoder \n '
                    'Press p to print position of encoder \n'
                    'Press d to print delta of encoder \n'
                    'Press g to start 30s of encoder data collection \n'
                    'Press s to end data collection \n'
                    'Press q to quit \n')
        if inv == 'z': # If value is a z, set position to zero
            state = 1
        elif inv == 'p': # If value is a p, print current position
            state = 2
        elif inv == 'd': # If value is a d, print current delta
            state = 3
        elif inv == 'g': # If value is a g, start 30s of data collection
            state = 4                   
        elif inv == 's': # If value is an s, end data collection
            state = 5      
        elif inv == 'q': # If value is a q, quit
            state = 6
                
    elif state == 1:
        print('z pressed')
        ser.write('z'.encode())
        state = 0
    
    elif state == 2:
        ser.write('p'.encode())
        print('p pressed')
        state = 0
        
    elif state == 3:
        ser.write('d'.encode())
        print('d pressed')
        state = 0
        
    elif state == 4:
        ser.write('g'.encode())
        print('g pressed')
        state = 0
        
    elif state == 5:
        ser.write('s'.encode())
        print('s pressed')
        state = 0
    
    elif state == 6:
        print('Bye')
        ser.close()
        KeyboardInterrupt
        break
        pass
            