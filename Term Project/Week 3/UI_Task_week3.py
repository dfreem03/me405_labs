# -*- coding: utf-8 -*-
""" @file UI_Task_week3.py
    @brief Reads serial input from PC
    @details Reads serial from PC and determines what data to get from shares_week3.py and what operations the hardware will perform
    @author: Daniel
"""

from pyb import UART
import shares_week3

#pyb.repl_uart(None) # Only use if using the py base board


class UI_Task:

    '''@brief UI_Task decides what to do with serial input
    @details UI_Task is a finite state machine class that decides what to do with serial input. It can set encoders, set motors, pull data and send data
    '''
    def __init__(self):
        '''@brief Constructor for UI task
        @details Creates object of shares.py class, establishes serial communication and sets the initial state
        '''
        self._shares = shares_week3
        self._myuart = UART(2)
        print('UI Task initialized')
        self._state = 0
        
    def run(self):
        '''@brief Runs the finite state machine
        '''
        if self.state == 0:
            if self.myuart.any() != 0:
                ## ASCII Value of input character
                val = self.myuart.readchar()
                if val == 122: # If value is a z, set position to zero
                    self.state = 1
                elif val == 112: # If value is a p, print current position
                    self.state = 2
                elif val == 100: # If value is a d, print current delta
                    self.state = 3
                elif val == 103: # If value is a g, start 30s of data collection
                    self.state = 4                   
                elif val == 115: # If value is an s, end data collection
                    self.state = 5                 
                    
        elif self.state == 1:
            print('z pressed')
            self._shares.zero
            self.state = 0
        
        elif self.state == 2:
            self.myuart.write(self._shares.encoder_position)
            print('p pressed')
            self.state = 0
            
        elif self.state == 3:
            self.myuart.write(self._shares.get_delta)
            print('d pressed')   
            self.state = 0
            
        elif self.state == 4:
            self._shares.mot_start = True
            self._shares.mot_stop = False
            print('g pressed')
            self.state = 0
            
        elif self.state == 5:
            self._shares.mot_start = False
            self._shares.mot_stop = True
            #self.myuart.write(shares.send_data)
            print('s pressed')
            self.state = 0
            
