# -*- coding: utf-8 -*-
"""@file shares_week3.py
@brief A place for EncoderTask.py to send data to and for UI_task_week3.py to pick data from
@details Creates data objects which are manipulatable by EncoderTask. UI_task is used to select which data items are to be presented
@author: Daniel Freeman
"""
## Gain of encoder
Kp = 1
## The current delta of the encoder
get_delta = None
## The current encoder position
encoder_position = None
## Zero position of the encoder, can be set to true by pushing z
zero = None
## The motor start command, can be set to true by pushing g
mot_start = None
## The motor stop command, can be set to true by pushing s
mot_stop = None

