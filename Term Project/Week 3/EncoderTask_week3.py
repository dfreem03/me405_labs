# -*- coding: utf-8 -*-
"""@file EncoderTask_week3.py
@brief This task handles controls between the UI task and the drivers, and sends data to shares
@details This task interfaces with shares and each driver. This task runs all drivers and is
        responsible for changing values based on user input. It also sends data from hardware to shares_week3.py \n
        Source code can be found at:
@author Daniel Freeman

"""
# Runs everything 
# Send everything to shares.py
# Pick what to print from shares.py

import shares_week3
import array
import CTRL_week3
import MotorDriver_week3
import EncoderDriver_week3
import utime

class CTRLtask:
    '''@brief Class to control system operations
    @details This class runs the motors, encoders and system control, and interfaces with the UI task and the shares file
    '''

    def __init__(self):
        ''' @brief Contructor to create objects from driver classes and import CSV data
        '''
        ## Time step to iterate through tasks
        self.step = 1000 
        ## Create object of ClosedLoop from CTRL class
        self.ClosedLoop = CTRL_week3.ClosedLoop()
        ## Create object of MotorDriver from MotorDriver class
        self.MotorDriver = MotorDriver_week3.MotorDriver()
        ## Create object of EncoderDriver from EncoderClass
        self.EncoderDriver = EncoderDriver_week3.EncoderDriver()
        ## Create object of array to store times
        #self.time_ref = array.array('f', 751*[0])
        self.time_ref = array.array('f', [])
        ## Create object of array to store reference omega values, in rpm
        self.omega_ref = array.array('f', [])
        ## Create object of array to store reference theta values, in encoder ticks
        self.theta_ref = array.array('f', [])
        ## Index for arrays
        self.i = 0
        with open('reference.csv', 'r') as self.csv_file:

            try:
                for line in self.csv_file:
                    ## Raw data from CSV File
                    self.string = self.csv_file.readline()
                    ## Data with line endings removed
                    self.strip = self.string.strip()
                    ## Data split on commas
                    self.split = self.strip.rsplit(',')
                    # self.time_ref[i] = (float(self.split[0]))
                    # self.omega_ref[i] = float(self.split[1])
                    # self.theta_ref[i] = float(self.split[2])
                    # i+=1
            except:
                pass
        ## Current type in program, ms
        self.thisTime = utime.ticks_ms()
        print('Creating control task')
        
    def run(self):
        '''@brief This function runs all the drivers, passes in values and sends data to main
        ''' 
        print('get_delta() = ' + str(self.EncoderDriver.delta))
        print('encoder position = ' + str(self.EncoderDriver.get_position))
        print('i = ' + str(self.i))

        ## Sends latest encoder position to shares file
        shares_week3.encoder_position = self.EncoderDriver.get_position()
        
        
        if utime.ticks_diff(utime.ticks_ms(), self.thisTime)>= 20:
            self.i+=1
            self.thisTime = utime.ticks_ms()
            # if self.i >= 751:
            #     self.i = 0
        if shares_week3.mot_start:
            self.ClosedLoop.set_Kp(shares_week3.Kp)
            self.omega_ref[self.i] = float(self.split[1])
            print('Encoder task - motor start')
            #print('omega_ref = ' + str(self.omega_ref))
            self.MotorDriver.enable()
            self.EncoderDriver.run()
            ## Measured encoder omega
            self.omega_meas = self.EncoderDriver.get_delta()/0.02/60
            shares_week3.mot_stop = False
            #for i in range(len(self.omega_ref)):
            self.ClosedLoop.run(self.omega_ref[self.i], self.omega_meas)
            self.MotorDriver.set_duty(self.ClosedLoop.PWM)
            shares_week3.encoder_position = self.EncoderDriver.position
        
        if shares_week3.zero:
            self.EncoderDriver.set_position(0)
            
        if shares_week3.get_delta:
            shares_week3.encoder_delta = self.EncoderDriver.get_delta()
            
        if shares_week3.mot_stop:
            self.MotorDriver.disable()
            shares_week3.mot_start = False
        
        
        
    
    
