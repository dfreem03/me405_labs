# -*- coding: utf-8 -*-
"""@file MotorDriver_week3.py
@brief Driver to run the motors
@details The driver is responsible for creating motor objects using pins and timers, setting up the motors for use and providing a duty cycle for the motor speed
@author: Daniel
"""

import pyb

class MotorDriver:
    '''@brief Class to create motor driver
    @brief The class allows for the motors to be enabled, to be set to a duty, and to disable.
    '''
    def __init__ (self):
        ''' @brief Creates a motor driver by initializing pins and timers
        '''
        print ('Creating a motor driver')
        ## PA15 Pin object to use as the enable pin
        self.PA15 = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT)
        ## IN1 A pyb.Pin object to use as the input to half bridge 1
        self.IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT)
        ## IN2 A pyb.Pin object to use as the input to half bridge 2
        self.IN2 = pyb.Pin(pyb.Pin.cpu.B5)
        ## tim3 A timer using timer 3 
        self.tim3 = pyb.Timer(3, freq = 20000)
        ## t3ch1 Channel 1 of timer 3
        self.t3ch1 =self.tim3.channel(1, pyb.Timer.PWM, pin = self.IN1)
        ## t3ch2 Channel 2 of timer 3
        self.t3ch2 =self.tim3.channel(2, pyb.Timer.PWM, pin = self.IN2)
        
               ## IN1 A pyb.Pin object to use as the input to half bridge 1
        self.IN3 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT)
        ## IN2 A pyb.Pin object to use as the input to half bridge 2
        self.IN4 = pyb.Pin(pyb.Pin.cpu.B1)
        ## tim7 A timer using timer 7 
        self.tim7 = pyb.Timer(7, freq = 20000)
        ## t7ch1 Channel 1 of timer 7
        self.t7ch1 =self.tim7.channel(1, pyb.Timer.PWM, pin = self.IN3)
        ## t7ch2 Channel 2 of timer 7
        self.t7ch2 =self.tim7.channel(2, pyb.Timer.PWM, pin = self.IN4)
        
        
    def enable (self):
        '''@brief Enables motor
        '''
        print ('Enabling Motor')
        self.PA15.high()

    def disable (self):
        '''@brief Disables motor
        '''
        print ('Disabling Motor')
        self.PA15.low()

    def set_duty (self, duty):
        ''' @brief This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
        print('Duty set to ' + str(duty))
        if duty >=0:
            self.t3ch1.pulse_width_percent(duty)
            self.t3ch2.pulse_width_percent(0)
        elif duty <0:

            self.t3ch2.pulse_width_percent(abs(duty))
            self.t3ch1.pulse_width_percent(0)
