# -*- coding: utf-8 -*-
"""@file CTRL_week3.py
@brief Provides the control of the MotorDriver_week3.py and EncoderDriver_week4.py
@details This task allows the user to set a gain value, Kp, which is used with the desired speed (from CSV) and the measured speed (from encoder) to give a new PWM which will be closer to the desired speed
@author: Daniel
"""

class ClosedLoop:
    '''@brief Closed Loop Controller Class
        @details Proportional controller for motor feedback
        '''
    def __init__(self):
        '''@brief Constructor for control task
        @details Sets an intial Kp gain value
        '''
        ## Gain
        self.Kp = 1
        
    def run(self, omega_ref, omega):
        '''@brief Runs the control task
        '''
        #print('CTRL Driver running')
        ## PWM value for motor in rpm 
        self.PWM = self.Kp*(omega_ref - omega)
        #Omega_ref is from csv
        #omega is the measured
        return self.PWM
        
    def get_Kp(self):
        '''@returns Kp value
        '''
        return self.Kp
    
    def set_Kp(self, val):
        '''@brief Allows the user to change the Kp value
        '''
        self.Kp = val
        