# -*- coding: utf-8 -*-
"""@file UI_week1.py
@brief User Interface for Data Collection
@details Asks user to press g to start data collection, s to stop, and presents data
@author: Daniel
"""
import serial
from matplotlib import pyplot
## Serial port
ser = serial.Serial(port='COM4',baudrate=115273,timeout=1)
## Current state of the finite state machine
state = 0
## Empty array for times
timeList = 301*[0]
## Empty array for data
dataList = 301*[0]
while True:
    if state == 0:
        ## Input character
        inv = input('Press g to start data 30 seconds of collection \n'
                    'Press s to end data collection \n'
                    'Press q to quit \n')
        if inv == 'g': # If value is a g, start 30s of data collection
            state = 1                  
        elif inv == 's': # If value is an s, end data collection
            state = 2     
        elif inv == 'q': # If value is a q, quit
            state = 3
        
    elif state == 1:
        ser.write('g'.encode())
        print('g pressed')
        state = 0
        # dataString = ser.readline().decode('ascii')
        # strippedStrings = dataString.strip()
        # splitStrings = dataString.split(',')
        # timeList.append(splitStrings[0])
        # dataList.append(splitStrings[1])
        # pyplot.figure()
        # #pyplot.plot(times, values)
        # pyplot.xlabel('Time')
        # pyplot.ylabel('Data')
        ## Counter to avoid waiting too long for response
        timeout = 0
        while ser.in_waiting == 0:
            timeout+=1
            if timeout > 1_000_000:
                state = 3
        
    elif state == 2:
        ser.write('s'.encode())
        print('s pressed')
        state = 0
        ## String of data from nucleo
        dataString = ser.readline().decode('ascii')
        ## Data with stripped parenthesis
        strippedStrings = dataString.strip()
        ## Data with striped commas
        splitStrings = dataString.split(',')
        timeList.append(splitStrings[0])
        dataList.append(splitStrings[1])
        pyplot.figure()
        #pyplot.plot(times, values)
        pyplot.xlabel('Time')
        pyplot.ylabel('Data')
        timeout = 0
        while ser.in_waiting == 0:
            timeout+=1
            if timeout > 1_000_000:
                state = 3
    
    elif state == 3:
        print('Bye')
        ser.close()
        KeyboardInterrupt
        break
        pass
    
    
            