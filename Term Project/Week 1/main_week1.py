# -*- coding: utf-8 -*-
"""@file main_week1.py
@brief File on nucleo 
@details File responsible for collecting data and sending data to PC
@author: Daniel
"""
import pyb
from pyb import UART
from math import exp, sin, pi
from array import array
import utime

#pyb.repl_uart(None) # Only use if using the py base board
## Serial port
myuart = UART(2)
## Array to store time data
times = array('f', 301*[0])
## Array to store collected data
values = array('f', 301*[0])
## Index
n = 0
## Initial state of FSM
state = 0 
## Timestamp for last iteration of the task in milliseconds
lastTime = utime.ticks_ms()

while True:
    if state == 0:   
        #print('state 0')
        if myuart.any() !=0:
            ## Timestamp for start of task
            thisTime = utime.ticks_ms()
            ## ASCII Value of serial character
            val = myuart.readchar() 
            if val == 103:
                state = 1

                                   
    elif state == 1:
        ## Timestamp for next iteration of the task
        nextTime = utime.ticks_ms()
        if utime.ticks_diff(utime.ticks_ms(), thisTime) <=30000:
            # print('thisTime = ' + str(thisTime))
            # print('nextTime = ' + str(nextTime))
            print('diff = ' + str(utime.ticks_diff(nextTime, thisTime)))
            print('mod =' + str(utime.ticks_diff(nextTime, thisTime) % 1000))
            values.append(exp(-times[n]/10)*sin((2*pi/3)*times[n]))
            print(n)
            if n == 30:
                state = 2
            if (utime.ticks_diff(nextTime, thisTime)) >=1000:
                n += 1  
                thisTime = utime.ticks_ms()

            if myuart.any():
                val = myuart.readchar() 
                if val == 115:
                    state = 2 
        else:
            print('Times up')
            state = 2
                    
    elif state == 2:
        print('state 2')
        #for n in range(len(times)):
        ## String of outbound serial data
        out_string = '{:}, {:} \r\n'.format(times, values)
        myuart.write(out_string.encode())
        while True:
            pass
        

    