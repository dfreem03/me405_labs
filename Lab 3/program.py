"""@file program.py
@brief Runs FSM3.py
@details Source code: https://bitbucket.org/dfreem03/me405_labs/src/master/Lab%203/program.py

@author: Daniel
"""

import FSM3

if __name__ == "__main__":
    ## Finite state machine task
    task1 = FSM3.FSM3(100)
    
    while True:
        try:
            task1.run()

            
        except KeyboardInterrupt:
            break
        
        
   