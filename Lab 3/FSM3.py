"""@file FSM3.py
   @brief Class Finite State Machine for Lab 0x03
   @details Prompts the user for button presses corresponding to a random sequence of LED flashes.
            As the user gets part of the sequence correct, more of the sequence will be revealed.
            Video available at https://www.youtube.com/watch?v=wXf7NvbQckg&feature=youtu.be
            Source code found at https://bitbucket.org/dfreem03/me405_labs/src/master/Lab%203/
   @image html IMG-9829.jpg
   @author Daniel Freeman
"""

import pyb
import utime
import random
import time

class FSM3:
    '''@brief Class for finite state machine
    '''
    # Define callback function as a class method
    def onButtonPress(self, IRQ_src):
        '''@brief Callback function
           @details Changes flag variable from zero to one as a result of an interrupt caused by a falling voltage on pin C13
           @param IRQ_src Voltage on pin C13
           @returns Changes flag variable from zero to one
        '''
        
        self.flag
        self.flag = 1
        
    def __init__(self,period):
        '''@brief Class initiation function
           @details Sets initial values for variables included in the class
           @param period Period of the program, set in the main file
        '''
        
        ## The current state of the finite state machine
        self.state = 0
        ## The current state of the flag variable
        self.flag = 0
        ## The current time
        self.time = 0
        ## Button pin
        #@brief pinC13 is the blue user button on the nucleo
        self.pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
        ## LED pin
        #@brief pinA5 is the LED on the nucleo
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        ## The button interrupt that triggers the callback function
        self.ButtonInt = pyb.ExtInt(self.pinC13, mode=pyb.ExtInt.IRQ_FALLING | pyb.ExtInt.IRQ_RISING, pull=pyb.Pin.PULL_NONE, callback=self.onButtonPress)
        ## Timestamp for last iteration of the task in microseconds
        #  could eliminate for concision if desired
        self.lastTime = utime.ticks_ms()
        ## The period for the task in microseconds
        self.period = period
        ## Timestamp for the next iteration of the task
        self.nextTime = utime.ticks_add(self.lastTime, self.period)
        ## The list of random numbers to make the sequence
        self.pattern = []
        ## Timer object
        #@brief Creates a timer object using timer 2, triggers at 20kHz
        self.tim2 = pyb.Timer(2, freq = 20000)
        ## Channel configure
        #@brief Configures channel 1 to toggle pinA5 using PWM from tim2
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)

    def transitionTo(self, newState):
        ''' @brief Tells the program to transition to the next state
        '''
        #print(str(self.state) + " to " + str(newState))
        self.state = newState
        
    def randomNumber(self):
        '''@brief Makes a random list
           @details Makes a random list consisting of five indexes with each index having a value of 1, 2 or 3
        '''
        self.pattern = [random.randrange(1,3), random.randrange(1,3), random.randrange(1,3), random.randrange(1,3), random.randrange(1,3)]
        
    def LED(self, period):
        '''@brief Generates LED pulses
           @details Takes the sequence from randomNumber and uses it to generate a random sequence of LED pulses
           @param period The finite state machine tells the function which part of the sequence to show
        '''
        if period == 1:
            time.sleep(0.25)
            self.t2ch1.pulse_width_percent(100)
            time.sleep(self.pattern[0])
            self.t2ch1.pulse_width_percent(0)
            
        elif period == 2:
            time.sleep(0.25)
            self.t2ch1.pulse_width_percent(100)
            time.sleep(self.pattern[0])
            self.t2ch1.pulse_width_percent(0)
            time.sleep(self.pattern[1])
            self.t2ch1.pulse_width_percent(100)
            time.sleep(self.pattern[2])
            self.t2ch1.pulse_width_percent(0)
            
            
        elif period == 3:
            time.sleep(0.25)
            self.t2ch1.pulse_width_percent(100)
            time.sleep(self.pattern[0])
            self.t2ch1.pulse_width_percent(0)
            time.sleep(self.pattern[1])
            self.t2ch1.pulse_width_percent(100)
            time.sleep(self.pattern[2])
            self.t2ch1.pulse_width_percent(0)
            time.sleep(self.pattern[3])
            self.t2ch1.pulse_width_percent(100)
            time.sleep(self.pattern[4])
            self.t2ch1.pulse_width_percent(0)

            
    def run(self):
        '''@brief Main program
           @details Takes the user through an introduction and then introduces parts of a random LED sequence,
           only advancing if they match the correct pause and pulse sequence
        '''
        ## The current time
        thisTime = utime.ticks_ms()
        if utime.ticks_diff(thisTime, self.nextTime) >= 0:
        ## Timestamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            
            if self.state == 0:
                print('Welcome! Push button for instructions')
                while self.flag == 0:
                    pass
                self.transitionTo(1)
                self.flag = 0
                
            elif self.state == 1:
                while self.flag == 0:
                    pass
                self.transitionTo(2)
                self.flag = 0
    
            elif self.state == 2:
                print('Push the button to reveal the first LED pulse in a random sequence. Then, hold the button for the '
                      'corresponding number of seconds that the LED is lit. If you get it right, more of the sequence will be loaded. Copy the LED pulses '
                      'with corresponding button pushes. There are three parts to the random pulse pattern. Good Luck! Push button to begin')
                while self.flag == 0:
                    pass
                self.transitionTo(3)
                self.flag = 0
                                               
            elif self.state == 3:
                self._time = utime.ticks_ms()
                while self.flag == 0:
                    pass
                self.transitionTo(4)
                self.flag = 0
                self._end = utime.ticks_ms()
                    
            elif self.state == 4:
                self._time2 = utime.ticks_ms()
                ## Time for button push
                self.pushTime = utime.ticks_diff(self._end,self._time)/1000
                self.flag = 0
                ## Rounded time of how long button was pushed for
                self.value = round(self.pushTime)
                #print(self.value)
                print('The first part of the pattern is ')
                self.randomNumber()
                self.LED(1)
                #print('LED was on for ' + str(self.pattern[0]) + ' seconds')
                print('Your turn!')
                while self.flag == 0:
                    pass
                self._end2 = utime.ticks_ms()
                self.transitionTo(5)
                self.flag = 0
                
            elif self.state == 5:
                self._time3 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end2,self._time2)/1000
                self.flag = 0
                while self.flag == 0:
                    pass
                self.transitionTo(6)
                self.flag = 0
                self._end3 = utime.ticks_ms()
                
            elif self.state == 6:
                self._time4 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end3,self._time3)/1000
                self.flag = 0
                self.value = round(self.pushTime)
                #print('Button was pressed for ' + str(self.value) + ' seconds')
                if self.value == self.pattern[0]:
                    print('Good job! The next part of the pattern is ')
                    self.LED(2)
                    #print('LED was on for ' +str(self.pattern[0]) +' seconds, off for '+str(self.pattern[1])+' seconds and on for '+str(self.pattern[2])+' seconds')
                    print('Your turn!')
                    while self.flag == 0:
                        pass
                    self.transitionTo(7)
                    self.flag = 0
                    self._end4 = utime.ticks_ms()
                else: 
                    print('First pulse wrong, unlucky! Push to restart.')
                    while self.flag == 0:
                        pass
                    self.transitionTo(23)
                    self.flag = 0
                    self._end4 = utime.ticks_ms()
                
            elif self.state == 7:
                self._time5 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end4,self._time4)/1000
                self.flag = 0
                while self.flag == 0:
                    pass
                self.transitionTo(8)
                self.flag = 0
                self._end5 = utime.ticks_ms()
                   
            elif self.state == 8:
                self._time6 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end5,self._time5)/1000
                self.flag = 0
                self.value = round(self.pushTime)
                if self.value == self.pattern[0]:
                    #print('Button was pressed for ' + str(self.value) + ' seconds')
                    print('First number correct')
                    self.transitionTo(9)
                    self.flag = 0
                    self._end6 = utime.ticks_ms()
                else:
                    print('Unlucky, first number was wrong. Push to restart.')
                    while self.flag == 0:
                        pass
                    self.transitionTo(23)
                    self.flag = 0
                    self._end6 = utime.ticks_ms()
                    
            elif self.state == 9:
                self._time7 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end6,self._time6)/1000
                self.flag = 0
                while self.flag == 0:
                    pass
                self.transitionTo(10)
                self.flag = 0
                self._end7 = utime.ticks_ms()
                    
            elif self.state == 10:
                self._time8 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end7,self._time7)/1000
                self.flag = 0
                self.value = round(self.pushTime)
                if self.value == self.pattern[1]:
                    #print('Button was released for ' + str(self.value) + ' seconds')
                    print('Pause correct')
                    self.transitionTo(11)
                    self.flag = 0
                    self._end8 = utime.ticks_ms()
                else:
                    print('Unlucky, pause was wrong. Push to restart.')
                    while self.flag == 0:
                        pass
                    self.transitionTo(23)
                    self.flag = 0
                    self._end8 = utime.ticks_ms()
                    
            elif self.state == 11:
                self._time9 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end8,self._time8)/1000
                self.flag = 0
                while self.flag == 0:
                    pass
                self.transitionTo(12)
                self.flag = 0
                self._end9 = utime.ticks_ms()
                          
            elif self.state == 12:
                self._time10 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end9,self._time9)/1000
                self.flag = 0
                self.value = round(self.pushTime)
                if self.value == self.pattern[2]:
                    print('Good job! The full pattern is ')
                    self.LED(3)
                    #print('LED was on for ' +str(self.pattern[0]) +' seconds, off for '+str(self.pattern[1])+' seconds, on for '+str(self.pattern[2])+' seconds, off for '+str(self.pattern[3])+ ' and on for '+str(self.pattern[4])+' seconds')
                    print('Your turn!')
                    while self.flag == 0:
                        pass
                    self.transitionTo(13)
                    self.flag = 0
                    self._end10 = utime.ticks_ms()
                else: 
                    print('Second pulse wrong, unlucky! Push to restart.')
                    while self.flag == 0:
                        pass
                    self.transitionTo(23)
                    self.flag = 0
                    self._end10 = utime.ticks_ms()
                        
            elif self.state == 13:
                self._time11 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end10,self._time10)/1000
                self.flag = 0
                while self.flag == 0:
                    pass
                self.transitionTo(14)
                self.flag = 0
                self._end11 = utime.ticks_ms()


            elif self.state == 14:
                self._time12 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end11,self._time11)/1000
                self.flag = 0
                self.value = round(self.pushTime)
                if self.value == self.pattern[0]:
                    print('First pulse correct')
                    self.transitionTo(15)
                    self.flag = 0
                    self._end12 = utime.ticks_ms()
                else:
                    print('Unlucky! Push to restart')
                    while self.flag == 0:
                        pass
                    self.transitionTo(23)
                    self.flag = 0
                    self._end12 = utime.ticks_ms()
                
            elif self.state == 15:
                self._time13 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end12,self._time12)/1000
                self.flag = 0
                while self.flag == 0:
                    pass
                self.transitionTo(16)
                self.flag = 0
                self._end13 = utime.ticks_ms()
                
            elif self.state == 16:
                self._time14 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end13,self._time13)/1000
                self.flag = 0
                self.value = round(self.pushTime)
                if self.value == self.pattern[1]:
                    print('First pause correct')
                    self.transitionTo(17)
                    self.flag = 0
                    self._end14 = utime.ticks_ms()
                else:
                    print('Unlucky! Push to restart')
                    while self.flag == 0:
                        pass
                    self.transitionTo(23)
                    self.flag = 0
                    self._end14 = utime.ticks_ms()    
                    
            elif self.state == 17:
                self._time15 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end14,self._time14)/1000
                self.flag = 0
                while self.flag == 0:
                    pass
                self.transitionTo(18)
                self.flag = 0
                self._end15 = utime.ticks_ms()
                
            elif self.state == 18:
                self._time16 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end15,self._time15)/1000
                self.flag = 0
                self.value = round(self.pushTime)
                if self.value == self.pattern[2]:
                    print('Second pulse correct')
                    self.transitionTo(19)
                    self.flag = 0
                    self._end16 = utime.ticks_ms()
                else:
                    print('Second pulse wrong, unlucky! Push to restart')
                    while self.flag == 0:
                        pass
                    self.transitionTo(23)
                    self.flag = 0
                    self._end16 = utime.ticks_ms()
            
            elif self.state == 19:
                self._time17 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end16,self._time16)/1000
                self.flag = 0
                while self.flag == 0:
                    pass
                self.transitionTo(20)
                self.flag = 0
                self._end17 = utime.ticks_ms()
                
            elif self.state == 20:
                self._time18 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end17,self._time17)/1000
                self.flag = 0
                self.value = round(self.pushTime)
                if self.value == self.pattern[3]:
                    #print('Button was released for ' + str(self.value) + ' seconds')
                    print('Second pause correct')
                    self.transitionTo(21)
                    self.flag = 0
                    self._end18 = utime.ticks_ms()
                else:
                     #print('Button was released for ' + str(self.value) + ' seconds')
                     print('Second pause wrong, unlucky! Push to restart')
                     while self.flag == 0:
                         pass
                     self.transitionTo(23)
                     self.flag = 0
                     self._end18 = utime.ticks_ms()    
                    
            elif self.state == 21:
                self._time19 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end18,self._time18)/1000
                self.flag = 0
                while self.flag == 0:
                    pass
                self.transitionTo(22)
                self.flag = 0
                self._end19 = utime.ticks_ms()
            
            elif self.state == 22:
                self._time20 = utime.ticks_ms()
                self.pushTime = utime.ticks_diff(self._end19,self._time19)/1000
                self.flag = 0
                self.value = round(self.pushTime)
                if self.value == self.pattern[4]:
                    #print('Button was pressed for ' + str(self.value) + ' seconds')
                    print('Third pulse correct. Game complete. Push to restart. Press ctrl+c to quit.')
                    while self.flag == 0:
                        pass
                    self.transitionTo(23)
                    self.flag = 0
                    self._end20 = utime.ticks_ms()
                else:
                    print('Third pulse wrong, unlucky! Push to restart')
                    while self.flag == 0:
                        pass
                    self.transitionTo(23)
                    self.flag = 0
                    self._end20 = utime.ticks_ms()    
            
            elif self.state == 23:
                self.flag = 0
                while self.flag == 0:
                    pass
                self.transitionTo(24)
                self.flag = 0

            elif self.state == 24:
                self.flag = 0
                while self.flag == 0:
                    pass
                self.transitionTo(0)
                self.flag = 0
                
                
                
                
                
                
                
                
                

        
        
        

    
    
