"""@file mcp9808.py
@brief Driver for MCP9808 sensor
@details Module contains an initializer to begin I2C communication and several methods to verify the sensor is attached. Module also
returns the measured ambient temperature, both in celsius and fahrenheit.It does this by reading the ambient temperature register of the sensor
and using an algorithm to convert the bytearray data to a useful temperature measurement
@author: Daniel Freeman
"""
import pyb
from pyb import I2C
import ustruct

_pB8 = pyb.Pin(pyb.Pin.cpu.B8)
_pB9 = pyb.Pin(pyb.Pin.cpu.B9)
## Establishes the microcontroller as the i2c master using bus 1
i2c = I2C(1, I2C.MASTER)

class coms:
    def __init__(self):
        '''@brief Initializes I2c
        @details Initializes the Nucleo as the master. Sets the i2c address to the default address of our sensor (0x18)
        '''
        i2c.init(I2C.MASTER, addr = 0x18, baudrate=400000)

    def check(self):
        '''@brief Verifies sensor is attached
        @details Verifies that the sensor is attached at the given bus address by reading two bytes from register 6 of address 0x18, which contains the hexadecimal form of the manufacturer ID. This ID is 0x0054. The bytes first be shifted using the 'ustruct' module
       @returns Manufacturer ID
        '''
        _values = i2c.mem_read(2, 0x18, 6)
        _data = ustruct.unpack('>H',_values)
        return _data[0]
    
    def celsius(self):
        '''@brief Gets ambient temperature in celsius
        @details Reads 2 bytes from register 5 of the sensor. These bytes are then split into the upper and lower byte, allowing for an 
        algorithm to convert the byte data into a signed temperature in degrees celsius. The algorithm is specified by the manufacturer
        @returns _c, temperature in degrees celsius
        '''
        _raw_c = i2c.mem_read(2, 0x18, 5)
        ## The upper most byte of data (msb) from the ambient temperature register
        upper = _raw_c[0]
        ## The lower most byte of data (lsb) from the ambient temperature register
        lower = _raw_c[1]
        upper = upper & 0x1F
        if ((upper&0x10)==0x10):
            upper = upper & 0x0F
            _c = 256 - (upper*16 + lower/16)
        else:
            _c = (upper*16 + lower/16)
        return _c

    def fahrenheit(self):
        '''@brief Gets ambient temperature in fahrenheit
        @details Calls the celsius function and converts the temperature to fahrenehit
        @returns _f, temperature in fahrenehit
        '''
        _c = self.celsius()
        _f = _c*1.8 + 32
        return _f

        
    
