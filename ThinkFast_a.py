"""@file ThinkFast_a.py
@brief This program tests a users reaction time
@details The program tests a users reaction time by waiting a random time between 2 and 3 seconds before lighting an LED. /n
Once the LED is lit, the user must press the blue user button. The users reaction time in seconds is the printed
@author: Daniel
"""

import pyb
import utime
import random

## Pin A5, the pin used with the LED
pA5 = pyb.Pin(pyb.Pin.cpu.A5)
## Pin C13, the pin used with the user button
pC13 = pyb.Pin(pyb.Pin.cpu.C13)

## The reaction time in microseconds
reaction = 0
_flag = 0
_state = 0

def utimeButtonPress(IRQ_src):
    """@brief Callback function
    @details Changes flag variable from zero to one as a result of an interrupt caused by a falling voltage on PB3
    """
    global _flag
    _flag = 1

## Interrupt to detect a falling edge on pin C13
utimeINT = pyb.ExtInt(pC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=utimeButtonPress)

while True:
    try:
        if _state == 0:
            pA5.low()
            print('Welcome to the reaction test. Push button to start')
            while _flag == 0:
                pass
            _state = 1
            _t = utime.ticks_us()
            _flag = 0
                
        elif _state == 1: 
            if utime.ticks_diff(utime.ticks_us(), _t) > random.randrange(2000000,3000000):
                _pushTime = utime.ticks_us()
                pA5.high()
                print('LED on')
                while _flag == 0:
                    pass
                print('Button pushed')
                reaction = utime.ticks_diff(utime.ticks_us(), _pushTime)
                _state = 2
                _flag = 0
                
        elif _state == 2:
            print('Your reaction time was ' + str(reaction/1000000) + ' seconds. Push to restart.')
            while _flag == 0:
                pass
            _state = 0
            _flag = 0
                
    except KeyboardInterrupt:
        break
    
pyb.ExtInt(pC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=None)
                
            
