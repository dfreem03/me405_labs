"""@file 0x03_UI.py
@brief User Interface for ADC reader
@details This file serves as the PC user interface to read ADC values from the nucleo. The file sends a serial 'g' to the \n
Nucleo and reads back the data. The data is then converted to voltage and milliseconds before being plotted. A csv file is 
also created. 
@author: Daniel Freeman
"""

import serial
import keyboard 
from matplotlib import pyplot
import csv

_state = 0
_last_key = ''
## An array to holt voltage data as floating point numbers
volts = [] 
## An array to hold time data as floating point numbers
time = [] 
## Reference voltage of ADC
v_ref = 3.3
## Resolution of ADC (2^n -1, where n is number of bits. In this case, n = 12)
res = 4095
    
def sendChar():
    '''@brief Function to send and recieve serial data to and from Nucleo
    @details This function is called when the user types 'g' on the keyboard. It sends an ASCII 'g' to the Nucleo and, \n
    once data is produced, reads the serial data. The data is then converted to floats and plotted.
    '''
    with serial.Serial('COM3',baudrate=115200,timeout=1) as ser:
        ser.write(str('g').encode())
        while ser.in_waiting == 0:
            pass
        for line in ser:
            ## The current line of data from the Nucleo, consisting of a time and an ADC value
            line = ser.readline().decode()
            [t,y] = line.strip().split(',')
            volts.append((float(y))*(v_ref/res))
            time.append(float(t)*1000)
            print('')    
        ser.close()

def callback(key):
    """ @brief Callback function which is called when a key has been pressed.
    @param key This parameter is the character of the input key
    @returns Returns the variable _last_key which represents the character of the pressed key
    """
    global _last_key
    _last_key = key.name

keyboard.on_release_key("g", callback=callback) 
while True:
    if _state == 0:
        print('Welcome. Press g to initialize the Nucleo to await a button press.')
        _state = 1     
    if _state == 1:
        if _last_key == 'g':
            print('g pushed')
            _state = 2
        else:
            pass      
    if _state == 2:
        print(sendChar())
        pyplot.figure()
        pyplot.plot(time,volts)
        pyplot.xlabel('Time (ms)')
        pyplot.ylabel('Voltage (V)')
        pyplot.title('Voltage vs time')
        with open('lab0x03.csv', 'w', newline='') as csvfile:
                csvwriter = csv.writer(csvfile)
                for i in range(len(volts)):
                    csvwriter.writerow([time[i], volts[i]])
        break


        
    
    
