"""@file 0x03_nucleo.py
@brief ADC data collection
@details This file runs on the nucleo and is used to collect ADC count data.
The file, upon receipt of a 'g' from the PC, starts filling a buffer array with ADC counts.
If a button push is detected (causing a change from 0 count to 4095 count) then the buffer array 
is bussed to the PC to be processed

@author: Daniel
"""
import pyb
import array
from pyb import UART

## Serial port
myuart = UART(2)
_state = 0
_pA0 = pyb.Pin(pyb.Pin.cpu.A0)
## Creates ADC object using pin A5
ADC = pyb.ADC(_pA0)
## Reads and returns the value on the analog pin
adc_val = ADC.read()
## Data storage array for ADC count
data = array.array('H',(0 for index in range(2500)))
## Timer object
tim = pyb.Timer(6,freq = 200000)
## Time per data point
dt = 1/200000

while True:
    if _state == 0:
        if myuart.any() != 0:
            _val = myuart.readchar()
            if _val == ord('g'):
                _state = 1
                
    if _state == 1:
        ADC.read_timed(data,tim)
        if data[-1] - data[0] > 4000:
            _state = 2
                
    if _state == 2:
        _idx = 0
        ## The time corresponding to the data point, in seconds
        time = 0
        while _idx < len(data):
            myuart.write('{:},{:} \r\n'.format(time, data[_idx]))
            time+=dt
            if _idx == len(data) - 1:
                myuart.deinit()
            else:
                _idx+=1          
        else:
            myuart.deinit()
            exit()
