"""
@file       shares.py
@brief      A file to help transition data between the UI and Control Task 
@details    Uses task_shares to ensure the data is not altered by several processes at once
@author     Mathis Weyrich
@date       Tue May 25 10:16:36 2021


"""
import task_share

R = task_share.Share('f', thread_protect = True, name = "R") # Resistance of the Motor
R.put(2.21)
Vdc = task_share.Share('i', thread_protect = True, name = "Vdc") # Steady state Voltage going to the motor
Vdc.put(12)
Kt = task_share.Share('f', thread_protect = True, name = "Kt") # Gain value for the motor - Kt = Tload/Vmotor (ask charlie)
Kt.put(13.8)

x = task_share.Share('f', thread_protect = True, name = "X") # X position of the ball
x.put(0)
xpred = task_share.Share('f', thread_protect = True, name = "Xpred") # Predicted X position of the ball
xpred.put(0)
x_d = task_share.Share('f', thread_protect = True, name = "X_d")# X velocity of the ball
x_d.put(0)
x_dpred = task_share.Share('f', thread_protect = True, name = "X_dpred")# Predicted X velocity of the ball
x_dpred.put(0)
y = task_share.Share('f', thread_protect = True, name = "Y") # Y position of the ball
y.put(0)
ypred = task_share.Share('f', thread_protect = True, name = "Ypred") # Predicted Y position of the ball
ypred.put(0)
y_d = task_share.Share('f', thread_protect = True, name = "Y_d") # Y velocity of the ball
y_d.put(0)
y_dpred = task_share.Share('f', thread_protect = True, name = "Y_dpred")# Predicted Y velocity of the ball
y_dpred.put(0)
thy = task_share.Share('f', thread_protect = True, name = "Thy") # Angle around y axis
thy.put(0)
thy_d = task_share.Share('f', thread_protect = True, name = "Thy_d") # Angular velocity around y axis
thy_d.put(0)
thx = task_share.Share('f', thread_protect = True, name = "Thx") # Angle around x axis
thx.put(0)
thx_d = task_share.Share('f', thread_protect = True, name = "Thx_d") # Angular velocity around x axis
thx_d.put(0)
z = task_share.Share('i', thread_protect = True, name = "z") # Ball on or off board
z.put(102)

pwmx = task_share.Share('f', thread_protect = True, name = "pwmx") # PWM for the x Motor
pwmx.put(0)
pwmy = task_share.Share('f', thread_protect = True, name = "pwmy") # PWM for the y Motor
pwmy.put(0)

fault = task_share.Share('i', thread_protect = True, name = "Fault") # Turns "t" if the motor gives off a fault (Will need the UI task to reset this variable to "f")
fault.put(102)#f in ascii = 102, t in ascii = 116
enable = task_share.Share('i', thread_protect = True, name = "Enable") # If the UI task changes this to "t", then the motor task will re-enable the motors - default is "f"
enable.put(102)

op_mode = task_share.Share('i', thread_protect = True, name = "OpMode") # The operating mode of the IMU - "I" = imu
op_mode.put(67) # C in ascii = 67, I = 73
calib = task_share.Share('i',name = "Calibration") # Calibration status of BNO
calib.put(102)#f in ascii = 102, t in ascii = 116

eulerx = task_share.Share('f', thread_protect = True, name = "EulerAngleX") # Euler angles returned from IMU
eulery = task_share.Share('f', thread_protect = True, name = "EulerAngleY") # Euler angles returned from IMU
eulerz = task_share.Share('f', thread_protect = True, name = "EulerAngleZ") # Euler angles returned from IMU as a vector of 3- [X, Y, Z]
omegax = task_share.Share('f', thread_protect = True, name = "AngVelX") # Angular velocity returned from IMu 
omegay = task_share.Share('f', thread_protect = True, name = "AngVelY") # Angular velocity returned from IMu 
omegaz = task_share.Share('f', thread_protect = True, name = "AngVelZ") # Angular velocity returned from IMu 


User_Input = task_share.Share('i', thread_protect = True, name = "UserInput") # The ASCII code for characters sent from the computer

dcol = task_share.Share('i', thread_protect = True, name = "DataCollection") # Default is f, UI turns to T when activated
dcol.put(102)

enc1_pos = task_share.Share('f', thread_protect = True, name = "Encoder1Pos") # Position of encoder 1 in radians
enc2_pos = task_share.Share('f', thread_protect = True, name = "Encoder2Pos") # Position of encoder 2 in radians
enc1_speed = task_share.Share('f', thread_protect = True, name = "Encoder1Speed") # Speed of encoder 1
enc2_speed = task_share.Share('f', thread_protect = True, name = "Encoder2Speed") # Speed of encoder 2
zero_enc = task_share.Share('i', thread_protect = True, name = "ZeroEnc") # Zero encoder 
zero_enc.put(102)

