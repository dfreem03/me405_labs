%% ME 405 HW0x04
% Daniel Freeman & Craig Belshe

%% Find Eigenvalues
syms s
I = eye(4);
p = vpa(det(s*I-A));
solve(p,s);

%% Define control using full state feedback
syms k1 k2 k3 k4 a0 a1 a2 a3 ks
k = [k1, k2, k3, k4];
Acl = vpa(A-B*k);
pcl = det(s*I-Acl);
charPol = collect(vpa(pcl));
coef = coeffs(charPol,s);
eqns = [a3 == coef(4)
        a2 == coef(3)
        a1 == coef(2)
        a0 == coef(1)];
[one,two,three,four] = (solve(eqns, k))  ;


%% Selecting a desired polynomial by choosing two pole, wn and zeta
syms s a1_ a2_ a3_ a0_ k1_ k2_ k3_ k4_
zeta = 0.9;
wn = 6*pi;
lambda3 = -30;
lambda4 = -40;
charPol1 = vpa(collect(((s^2 + 2*zeta*wn*s +wn^2)*(s-lambda3)*(s-lambda4)),s));
rooty = double(roots(coeffs(s^2+2*zeta*wn*s+wn^2)));
p_s_c = coeffs(charPol1);
%% New gains
a0_ = p_s_c(1);
a1_ = p_s_c(2);
a2_ = p_s_c(3);
a3_ = p_s_c(4);
k1_ = subs(one,[a0 a1 a2 a3], [a0_ a1_ a2_ a3_]);
k2_ = subs(two,[a0 a1 a2 a3], [a0_ a1_ a2_ a3_]);
k3_ = subs(three,[a0 a1 a2 a3], [a0_ a1_ a2_ a3_]);
k4_ = subs(four,[a0 a1 a2 a3], [a0_ a1_ a2_ a3_]);
k_ = double([k1_ k2_ k3_ k4_]);

%% Plots
figure(2)
subplot(2,2,1)
plot(out.tout,out.simout5(:,1))
xlabel('Time, seconds')
ylabel('Ball displacement from center, meters')
title('Tuned system - displacement')
subplot(2,2,2)
plot(out.tout,out.simout5(:,2))
xlabel('Time, seconds')
ylabel('Plate angle, radians')
title('Tuned system - plate angle')
subplot(2,2,3) 
plot(out.tout,out.simout5(:,3))
xlabel('Time, seconds')
ylabel('Ball velocity, m/s')
title('Tuned system - velocity')
subplot(2,2,4)
plot(out.tout,out.simout5(:,4))
xlabel('Time, seconds')
ylabel('Plate angular velocity, rad/s')
title('Tuned system - plate angular velocity')





