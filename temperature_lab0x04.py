'''@file temperature_lab0x04.py
@brief Module to take temperature readings and save as .csv
@details This file uses the mcp9808 module and the ADC module to take temperature readings from the
microcontroller and from the MCP9808 sensor, and saves them as a .csv file. TThe program first checks that the right sensor is attached by comparing the
found manufacturer ID with a user specified manufacturer ID, before collecting temperature data at a set interval for a set duration. 
One the data collection is complete, a csv file is written, accessible by re-inserting the usb cable. 
@author: Daniel Freeman
'''
import mcp9808
import ADC
import utime
import array

## MCP9808 object
sensor = mcp9808.coms()
## Object for internal temperature measurement
internal_temp = ADC.internal()
_state = 0
## Manufacturer ID of desired sensor
address = 84
## Interval for data collection, in milliseconds
interval = 60000
## Overall time for data collection, set by number of intervals (ms)
duration = 60*interval
## Array for microcontroller internal temperature storage
stm32 = array.array('f',[])
## Array for ambient temperature storage in celsius
amb_c = array.array('f',[])
## Array for ambient temperature storage in fahrenheit
amb_f = array.array('f',[])
_point = 0
## Array for times, in seconds
times = array.array('f',[])

if __name__ =='__main__':
    while True:
        try:
            if _state == 0:
                if sensor.check() == address:
                    print('Correct sensor attached')
                    _start = utime.ticks_ms()
                    _begin = utime.ticks_ms()
                    _state = 1
                else:
                    print('Incorrect sensor attached')
                    
            if _state == 1:
                while utime.ticks_diff(utime.ticks_ms(),_begin) < duration: 
                    if utime.ticks_ms() - _start == interval:
                        print('Data point ',_point,' out of ', duration/interval,' collected')
                        times.append(10+_point*10)
                        _point +=1
                        stm_temp = internal_temp.internal()
                        stm32.append(stm_temp)
                        mcp_temp_c = sensor.celsius()
                        amb_c.append(mcp_temp_c)
                        mcp_temp_f = sensor.fahrenheit()
                        amb_f.append(mcp_temp_f)
                        _start = utime.ticks_ms()       
                _state = 2
            
            if _state == 2:
                print('Data collection complete.')
                with open('lab0x04.csv',"w") as file:
                    for i in range(len(stm32)):
                        file.write('{:},{:},{:}\n'.format(times[i],stm32[i], amb_c[i]))
                break
        except KeyboardInterrupt:
            print("Thanks!")
            break
            


