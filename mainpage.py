# -*- coding: utf-8 -*-
"""@file mainpage.py

@mainpage

@section sec_port Portfolio Details
Welcome to my portfolio for the Cal Poly Mechatronics series. Here you will find the links to documentation of my code for each
lab, as well as some images for finite state machines. Links for the source code are also presented.

Please click to be taken to each module:
* ME 305 - HW 0x02 (\ref sec_hw2)
* ME 305 - Lab 0x01 (\ref sec_lab1) 
* ME 305 - Lab 0x02 (\ref sec_lab2) 
* ME 305 - Lab 0x03 (\ref sec_lab3) 
* ME 305 - Lab 0xFF, Week 1 (\ref sec_week1) 
* ME 305 - Lab 0xFF, Week 2 (\ref sec_week2) 
* ME 305 - Lab 0xFF, Week 3 (\ref sec_week3) 
* ME 305 - Lab 0xFF, Week 4 (\ref sec_week4) 
* ME 405 - HW 0x01  (\ref sec_hw1405)
* ME 405 - Lab 0x01 (\ref sec_lab1405)

@section sec_hw2 HW 0x02 Documentation
This homework assignment asked us to create and program a finite state machine for a two story elevator.

Documentation for HW 0x02: HW0x02.py \n
Source code for HW 0x02: https://bitbucket.org/dfreem03/me405_labs/src/master/HW0x02/ \n
Finite State Machine for HW 0x02: \n
    @image html hw0x02.jpg width = 40%

@section sec_lab1 Lab 0x01 Documentation
Lab 1 was our first real use of Python. We created a program to give the Fibonacci number of any corresponding index given by the user.

Documentation for Lab 1: Fibonacci.py \n
Source code for Lab 1: https://bitbucket.org/dfreem03/me405_labs/src/master/Lab1/

@section sec_lab2 Lab 0x02 Documentation
This lab focused on button interrupts. We used the user button on the nucleo to cycle between three different LED blinking patterns. We also used the hardware timers to create the patterns.

Documentation for Lab 2: LAB0x02.py \n
Source code for Lab 2: https://bitbucket.org/dfreem03/me405_labs/src/master/LAB0x02.py \n
Finite State Machine for lab 2:
    @image html lab0x02.jpg width = 50%
    
@section sec_lab3 Lab 0x03 Documentation
In lab 3 we created a 'Simon Says' type game, where the user must push the user button on the nucleo in sequence with the preceeding random LED pattern. If they are correct, the user advances.
If they are incorrect, they get to restart or quit. This lab also uses the button interrupts and timers. Program is made as a class and a main script.

Documentation for Lab 3 class: FSM3.py \n
Documentation for main script: program.py \n
Source code for Lab 3: https://bitbucket.org/dfreem03/me405_labs/src/master/Lab%203/ \n
Gameplay video: https://www.youtube.com/watch?v=wXf7NvbQckg&feature=youtu.be \n
Finite State Machine for lab 3:
    @image html lab0x03.jpg width = 50%

@section sec_week1 Lab 0xFF, Week 1 Documentation
This section is for week 1 of lab 0xFF, which is our term project. Week 1 involved sending serial input to our nucleo and sending data back to the PC. 
The goal was to send a 'g' to start 30s of data collection, for the nucleo to run a math function for 30s and after 30s (or 's' was pressed) the nucleo 
would send the data back to the PC. We were then tasked to print the data as a plot and to a CSV. 

I was able to send serial communication to the nucleo, record for 30s and to push 's' to stop at a desired interval, however, I was unable to print the
data as I was having trouble with memory allocation in my arrays. With a little more help and guidance, I am confident that these issuse could easily be solved.

Documentation for PC User Interface: UI_week1.py \n
Documentation for nucelo data collection: main_week1.py\n
Source code for PC User Interface: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%201/UI_week1.py \n
Source code for nucleo data collection: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%201/main_week1.py \n
Image of finite state machine for user interface: \n
    @image html UI_week1.jpg width = 50%
Image of finite state machine for data collection: \n
    @image html data_week1.jpg width = 50%

@section sec_week2 Lab 0xFF, Week 2 Documentation
This section is for week 2 of lab 0xFF. We were challenged to continue our serial communication work with the encoders. We had to create encoder drivers used to 
measure the position of the shaft, and then serially communicate to the nucleo to print data or transmit positional commands. 

For this task, I was able to create the encoder driver so that encoder ticks would increase or decrease based on shaft rotation and create a UI to send commands to the nucelo. I was unable to transmit this 
data back to the PC and create plots.

Documentation for PC User Interface: UI_week2.py \n 
Documentation for encoder driver: EncoderDriver_week2.py \n
Documentation for main nucleo file: main_week2.py \n
Source code for PC User Interface: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%202/UI_week2.py \n
Source code for encoder driver: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%202/EncoderDriver_week2.py \n
Source code for main nucleo file: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%202/main_week2.py \n
Image of finite state machine for user interface: \n
    @image html UI_week2.jpg width = 50%
Image of finite state machine for data collection:
    @image html data_week2.jpg width = 50%

@section sec_week3 Lab 0xFF, Week 3 Documentation
This section is for week 3 of lab 0xFF. Week 3 builds off the previous weeks as we were faced with adding the motors in to our encoder set up. We first had to make a 
motor driver to initalize the motors. We then had to combine this with the encoder to create a feedbback loop. This is done using the control driver. The control driver uses a closed loop proportional controller,
meaning that the PWM of the motor was related to the difference in desired motor speed and actual motor speed by a gain value, which can be set by the user. We also had to 
create a control task, which would interface between the motor and encoder drivers allowing for hardware communication. 

Documentation for User Interface Task: UI_Task_week3.py \n
Documentation for Encoder Driver: EncoderDriver_week3.py \n
Documentation for Control Task: EncoderTask_week3.py \n
Documentation for Motor Driver: MotorDriver_week3.py \n
Documentation for shares file: shares_week3.py \n
Documentation for Control Driver: CTRL_week3.py \n
Source code for User Interface Task: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%203/UI_Task_week3.py \n
Source code for Encoder Driver: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%203/EncoderDriver_week3.py \n
Source code for Control Task: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%203/EncoderTask_week3.py \n
Source code for Motor Driver: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%203/MotorDriver_week3.py \n
Source code for shares file: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%203/shares_week3.py \n
Source code for Control Driver: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%203/CTRL_week3.py

@section sec_week4 Lab 0xFF, Week 4 Documentation
For the final week of the term project, we had to import a CSV file giving a pre-determined motor routine and copy it. I was able to import the file, read the data and send 
it to the nucleo, however was unsucessful with producing any data.

Documentation for User Interface Task: UI_Task_week4.py \n
Documentation for Encoder Driver: EncoderDriver_week4.py \n
Documentation for Control Task: EncoderTask_week4.py \n
Documentation for Motor Driver: MotorDriver_week4.py \n
Documentation for shares file: shares_week4.py \n
Documentation for control driver: CTRL_week4.py \n
Source code for User Interface Task: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%203/UI_Task_week3.py \n
Source code for Encoder Driver: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%203/EncoderDriver_week3.py \n
Source code for Control Task: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%203/EncoderTask_week3.py \n
Source code for Motor Driver: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%203/MotorDriver_week3.py \n
Source code for shares file: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%203/shares_week3.py \n
Source code for Control Driver: https://bitbucket.org/dfreem03/me405_labs/src/master/Term%20Project/Week%204/CTRL_week4.py

@section sec_hw1405

@section sec_lab1405
Our first ME 405 lab involved creating code for a Vendotron vending machine. The vending machine should accept any currency from \n
pennies to twenty dollar bills, the user should be able to select from 4 different beverages, and the user should be able to vend \n
the selected beverage if they have enough money. If not, the vending machine will ask for more. If the user provides too much money, \n
the vending machine will return change using the smallest amount of currency possible, or ask if they would like a second beverage. \n
The user may also select to eject their money at any time by pushing 'e'.

Documentation for Vendotron: vend.py
Source code: 
Finite State Machine diagram:
    @image html



@author: Daniel Freeman
"""

