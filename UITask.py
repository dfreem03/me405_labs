"""
@file       UI_task.py
@brief      Interfaces directly with the computer for the nucleo

@author     Mathis Weyrich
@date       Sat May  27 15:38:24 2021
"""


# Use the Com _port 6


from pyb import USB_VCP

import shares,pyb, task_share, cotask, print_task, utime

    
def UI_task():
    '''
    @brief      Uses a virtual comms _port for user data input
    @details    Pressing the 'e' key on the keyboard will re-enable the motor,
                pressing 'i' will cause the BNO to be configured in IMU mode, 
                and pressing 'c' will reset the encoder position to 0. 
    '''
    _port = USB_VCP() # Initializes a virtual comms _port
    _state = 0
    
    _usrin = 0
    
    
    while True:
        #if _state == 0:
                        
            
        if _port.any(): # Reading inputs from the Computer
        
            _usrin = int(_port.read(1)[0]) #To read the first character in the port
            shares.User_Input.put(_usrin)
            #print('user Input = ' + str(shares.User_Input))
            if shares.User_Input.get() == 65 or shares.User_Input.get() == 101:#E
                shares.enable.put(116) # Re-enabling the motor
                print_task.put_bytes(b'Enabling the motor\r\n')
                shares.User_Input.put(0)
            if shares.User_Input.get() == 69 or shares.User_Input.get() == 105:#I
                shares.op_mode.put(73) # Sets BNO chip to IMU mode 
                print_task.put_bytes(b'Setting the BNO to IMU mode\r\n')
                shares.User_Input.put(0)
            if shares.User_Input.get() == 67 or shares.User_Input.get() == 99:#C
                shares.zero_enc.put(116) # Resets the encoder position
                print_task.put_bytes(b'Reseting position\r\n')
                shares.User_Input.put(0)
            if shares.User_Input.get() == 61 or shares.User_Input.get() == 61: #a - this is a motor test
                shares.pwmx.put(30)
                utime.sleep(0.05)
                shares.pwmx.put(0)
                shares.User_Input.put(0)
           
                                                        
        if shares.fault.get() == 116: # Taking inputs and sending it to the computer
            shares.fault.put(102) # Resets the fault value
            print_task.put_bytes(b'Press E to reset the motor\r\n')
                
        
                  
                
        yield(0)    
                

                        
uitask = cotask.Task (UI_task, name = 'UITask', priority = 1, 
                         period = 35, profile = True, trace = False)
cotask.task_list.append (uitask)             
    
            
        

            
            
        