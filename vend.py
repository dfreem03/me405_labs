"""@file vend.py
@brief Vending Machine Simulator
@details This program simulates vending machine operation. The program prompts the user to insert money corresponding to numbers on the keyboard. \n
If a sufficient amount of money is inserted, a drink will be vended. Drinks are also selected based on character inputs on the keyboard. \n
If there is change after the purchase, the machine asks the user if they would like to select another drink. \n
If the user take too long to insert their money or make a decision, the machine will prompt the user to insert money. \n
The user can push 'e' at any time to eject their money. They can also push 'q' to quit the program.
@date Apr. 8 2021
@author: Daniel Freeman
"""

import keyboard # Imports keyboard module
import time # Import time module

## @brief Price of Popsi in cents 
popsi = 120
## @brief Price of Cuke in cents
cuke = 100
## @brief Price of Dr Pupper in cents
drpupper = 110
## @brief Price of Spryte in cents
spryte = 85
_last_key = ''
_state = 0
## @brief Initial balance 
balance = 0
_selection = 0
_start = time.time() 

def getChange(price, payment):
    '''@brief This function computes the required change
    @param price This parameter is an integer representing the number of cents that the item costs
    @param payment This parameter is an integer representing the current balance of the user in cents
    @return Returns the required change from the transaction in the smallest amount of curreny possible using a list to store quantity of each currency type
    '''
    ## The amount of change required, in cents
    global change
    change = payment - price
    ## @brief The integer number of twenty dollar bills required
    twenty = int(change/2000)
    change = change - (twenty*2000) # Takes the number of twenty dollar bills off the total change
    ## @brief The integer number of ten  dollar bills required
    ten = int(change/1000)
    change = change - (ten*1000) # Takes the number of ten dollar bills off the total change 
    ## @brief The integer number of five dollar bills required 
    five = int(change/500)
    change = change - (five*500) # Takes the number of five dollar bills off the total change
    ## @brief The integer number of one dollar bills required
    dollar = int(change/100)
    change = change - (dollar*100) # Takes the number of one dollar bills off the total change
    ## @brief The integer number of quarters required
    quarter = int(change/25)
    change = change - (quarter*25) # Takes the number of quarters off the total change
    ## @brief The integer number of dimes required
    dime = int(change/10)
    change = change - (dime*10) # Takes the number of dimes off the total change
    ## @brief The integer number of nickels required
    nickel = int(change/5)
    ## @brief The integer number of pennies required
    penny = change - (nickel*5) 
    ## @brief Creates a list to store change values
    change = [penny, nickel, dime, quarter, dollar, five, ten, twenty]
    return change
 
def kb_cb(key):
    """ @brief Callback function which is called when a key has been pressed.
    @param key This parameter is the character of the input key
    @returns Returns the variable _last_key which represents the character of the pressed key
    """
    global _last_key
    _last_key = key.name
    
keyboard.on_release_key("c", callback=kb_cb) # User wants a cuke
keyboard.on_release_key("p", callback=kb_cb) # User wants a popsi
keyboard.on_release_key("d", callback=kb_cb) # User wants a dr pupper
keyboard.on_release_key("s", callback=kb_cb) # User wants a spryte
keyboard.on_release_key("q", callback=kb_cb) # User wants to quit
keyboard.on_release_key("e", callback=kb_cb) # User wants to eject coins
keyboard.on_release_key("1", callback=kb_cb) # User inserted a penny
keyboard.on_release_key("2", callback=kb_cb) # User inserted a nickel
keyboard.on_release_key("3", callback=kb_cb) # User inserted a dime
keyboard.on_release_key("4", callback=kb_cb) # User inserted a quarter
keyboard.on_release_key("5", callback=kb_cb) # User inserted a dollar
keyboard.on_release_key("6", callback=kb_cb) # User inserted a five
keyboard.on_release_key("7", callback=kb_cb) # User inserted a ten
keyboard.on_release_key("8", callback=kb_cb) # User inserted a twenty

if __name__ == '__main__':
    while True:
        if _state == 0:
            print('Welcome to the coolest vending machine ever. Please insert coins by pressing the corresponding numbers on the keyboard \n'
                  'penny = 1 \n'
                  'nickel = 2 \n'
                  'dime = 3 \n'
                  'quarter = 4 \n'
                  'dollar = 5 \n'
                  'five dollars = 6 \n'
                  'ten dollars = 7 \n'
                  'twenty dollars = 8 \n'
                  '\n'
                  'To select a beverage, please use the following key commands \n'
                  'Cuke = c ($1.00)\n'
                  'Popsi = p ($1.20) \n'
                  'Spryte = s ($0.85)\n'
                  'Dr. Pupper = d ($1.10)\n'
                  'Press e to eject your money \n'
                  'Press q at any time to leave the vending machine.')
            _state = 1
            
            
        elif _state == 1: # If _statement for _state 1 - this _state is where the FSM waits for input and decides what to send to the vending _state
            _run = time.time() - _start 
            if _run > 30:
                print('Dont be shy, give me your money!')
                _start = time.time()  # Reset _run timestamp
            if _last_key == '1': 
                balance += 1 # Increase balance by 1 cent
                print('Inserted 1 cent. Current balance: ' +str(balance) + 'cents')
                _last_key = '' # Reset last key flag
                _start = time.time()  # Reset _run timestamp
            elif _last_key == '2':
                balance += 5 # Increase balance by 5 cents
                print('Inserted 5 cents. Current balance: ' +str(balance) + 'cents')
                _last_key = ''
                _start = time.time()  # Reset _run timestamp
            elif _last_key == '3':
                balance += 10 # Increase balance by 10 cents
                print('Inserted 10 cents. Current balance: ' +str(balance) + 'cents')
                _last_key = ''
                _start = time.time()  # Reset _run timestamp
            elif _last_key == '4':
                balance += 25 # Increase balance by 25 cents
                print('Inserted 25 cents. Current balance: ' +str(balance) + 'cents')
                _last_key = ''
                _start = time.time()  # Reset _run timestamp
            elif _last_key == '5':
                balance += 100 # Increase balance by 100 cents
                print('Inserted 1 dollar. Current balance: ' +str(balance) + 'cents')
                _last_key = ''
                _start = time.time()  # Reset _run timestamp
            elif _last_key == '6':
                balance += 500 # Increase balance by 500 cents
                print('Inserted 5 dollars. Current balance: ' +str(balance) + 'cents')
                _last_key = ''
                _start = time.time()  # Reset _run timestamp
            elif _last_key == '7':
                balance += 1000 # Increase balance by 1000 cents
                print('Inserted 10 dollars. Current balance: ' +str(balance) + 'cents')
                _last_key = ''
                _start = time.time()  # Reset _run timestamp
            elif _last_key == '8':
                balance += 2000 # Increase balance by 2000 cents
                print('Inserted 20 dollars (wow). Current balance: ' +str(balance) + 'cents')
                _last_key = ''
                _start = time.time()  # Reset _run timestamp
            elif _last_key == 'p':
                print(' Popsi selected \n')
                _last_key = ''
                _start = time.time()  # Reset _run timestamp
                if balance >= popsi:
                    _selection = 1 # Change _selection to represent popsi
                    _state = 2
                else:
                    print('Insufficient balance. Please insert more money')
            elif _last_key == 'c':
                _last_key = ''
                _start = time.time()  # Reset _run timestamp
                print('Cuke selected \n')
                if balance >= cuke:
                    _selection = 2 # Change _selection to represent cuke
                    _state = 2
                else:
                    print('Insufficient balance. Please insert more money')
            elif _last_key == 's':
                _last_key = ''
                _start = time.time()  # Reset _run timestamp
                print('Spryte selected \n')
                if balance >= spryte:
                    _selection = 3 # Change _selection to represent spryte
                    _state = 2
                else:
                    print('Insufficient balance. Please insert more money')
            elif _last_key == 'd':
                _last_key = ''
                _start = time.time()  # Reset _run timestamp
                print('Dr. Pupper selected \n')
                if balance >= drpupper:
                    _selection = 4 # Change _selection to represent dr pupper
                    _state = 2
                else:
                    print('Insufficient balance. Please insert more money')
            elif _last_key == 'e':
                balance = 0
                print('Your money has been ejected. \n'
                      'Current balance is: ' + str(balance) + ' cents.')
                _last_key = ''
                _start = time.time()
            elif _last_key == 'q':
                _last_key = ''
                _start = time.time()  # Reset _run timestamp
                print('You left the vending machine. Goodbye')
                keyboard.unhook_all ()
                break
            
        elif _state == 2:
            if _selection == 1:
                _selection = 0 # Reset _selection flag
                getChange(popsi,balance) # _run change function
                if sum(change) > 0:
                    print('Thanks for buying a Popsi. Your change is: ' + str(change[0]) + ' pennies, ' + str(change[1]) + ' nickels, ' + str(change[2]) + ' dimes, ' + str(change[3]) + ' quarters, ' + str(change[4]) + ' dollars, ' + str(change[5]) + ' fives, ' + str(change[6]) + ' tens, and ' + str(change[7]) + ' twenties \n'
                          'That is also known as ' + str(change[0] + change[1]*5 + change[2]*10 + change[3]*25 + change[4]*100 + change[5]*500 + change[6]*1000 + change[7]*2000) + ' cents. \n'
                          'If you want, make another beverage selection. More money may be required. Press e to get your change. Press q to quit.')
                    balance -= popsi  # Update balance
                    _state = 1

            elif _selection == 2:
                _selection = 0
                getChange(cuke,balance)
                if sum(change) > 0:
                    print('Thanks for buying a Cuke. Your change is: ' + str(change[0]) + ' pennies, ' + str(change[1]) + ' nickels, ' + str(change[2]) + ' dimes, ' + str(change[3]) + ' quarters, ' + str(change[4]) + ' dollars, ' + str(change[5]) + ' fives, ' + str(change[6]) + ' tens, and ' + str(change[7]) + ' twenties \n'
                          'That is also known as ' + str(change[0] + change[1]*5 + change[2]*10 + change[3]*25 + change[4]*100 + change[5]*500 + change[6]*1000 + change[7]*2000) + ' cents. \n' 
                          'If you want, make another beverage selection. More money may be required. Press e to get your change. Press q to quit.')
                    balance -= cuke  # Update balance
                    _state = 1

            elif _selection == 3:
                _selection = 0
                getChange(spryte,balance)
                if sum(change) > 0:
                    print('Thanks for buying a Spryte. Your change is: ' + str(change[0]) + ' pennies, ' + str(change[1]) + ' nickels, ' + str(change[2]) + ' dimes, ' + str(change[3]) + ' quarters, ' + str(change[4]) + ' dollars, ' + str(change[5]) + ' fives, ' + str(change[6]) + ' tens, and ' + str(change[7]) + ' twenties \n'
                          'That is also known as ' + str(change[0] + change[1]*5 + change[2]*10 + change[3]*25 + change[4]*100 + change[5]*500 + change[6]*1000 + change[7]*2000) + ' cents. \n'
                          'If you want, make another beverage selection. More money may be required. Press e to get your change. Press q to quit.')
                    balance -= spryte  # Update balance
                    _state = 1

            elif _selection == 4:
                _selection = 0
                getChange(drpupper,balance)
                if sum(change) > 0:
                    print('Thanks for buying a Dr Pupper. Your change is: ' + str(change[0]) + ' pennies, ' + str(change[1]) + ' nickels, ' + str(change[2]) + ' dimes, ' + str(change[3]) + ' quarters, ' + str(change[4]) + ' dollars, ' + str(change[5]) + ' fives, ' + str(change[6]) + ' tens, and ' + str(change[7]) + ' twenties \n'
                          'That is also known as ' + str(change[0] + change[1]*5 + change[2]*10 + change[3]*25 + change[4]*100 + change[5]*500 + change[6]*1000 + change[7]*2000) + ' cents. \n'
                          'If you want, make another beverage selection. More money may be required. Press e to get your change. Press q to quit.')
                    balance -= drpupper  # Update balance
                    _state = 1

    
    


