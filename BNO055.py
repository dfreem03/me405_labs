"""

@file       BNO055.py
@brief      Uses the I2C to communicate with the IMU unit
@details    The program provides the capability of communicating with the IMU sensor 
            and returns angles, rotational velocity, and system status
@author     Mathis Weyrich            
@Date       Thu May 20 12:49:51 2021
@copyright  License Info

"""
# Offset angles: Pitch offset = -1.0625, Roll offset = -0.1875
# offset omega: Theta Y = -0.0625


import pyb, ustruct, time

class BNO:
    # i2c = I2C(1, I2C.MASTER) - This needs to go in the code before calling this function
    # i2c.init(I2C.MASTER)
    
    # Global address = 40
    
    # Chip ID = 0xA0 - from addr--> 0x00
    # Accelerator ID = 0xFB - from addr --> 0x01
    # Mag ID = 0x32 - from addr --> 0x02
    # Gyro ID = 0x0F - from addr --> 0x03
    # Accel X MSB addr--> 0x09
    # Accel X LSB addr--> 0x08
    # Accel Y MSB addr--> 0x0B
    # Accel Y LSB addr--> 0x0A
    # Accel Z MSB addr--> 0x0D
    # Accel Z LSB addr--> 0x0C
    # Gyro X MSB addr--> 0x15
    # Gyro X LSB addr--> 0x14
    # Gyro Y MSB addr--> 0x17
    # Gyro Y LSB addr--> 0x16
    # Gyro Z MSB addr--> 0x19
    # Gyro Z LSB addr--> 0x18
    # Euler X MSB addr --> 0x1B
    # Euler X LSB addr --> 0x1A
    # Euler Y MSB addr --> 0x1D
    # Euler Y LSB addr --> 0x1C
    # Euler Z MSB addr --> 0x1F
    # Euler Z LSB addr --> 0x1E
    
    # Unit Select- addr --> 0x3B
        # Bit 3 - Euler units (0 = degrees, 1 = radians)
        # Bit 2 - Gyr Units (0 = dps, 1 = rds)
        
    # Calibration status addr --> 0x35
        # MAG: If bit 0+1 = 11 => calibrated, 00 => uncalibrated
        # ACC: If bit 2+3 = 11 => calibrated, 00 => uncalibrated
        # GYR: If bit 4+5 = 11 => calibrated, 00 => uncalibrated
        # SYS: If bit 6+7 = 11 => calibrated, 00 => uncalibrated
        
    # Operating mode addr = 0x3D
    
    
    def __init__ (self,I2C): #Takes a fully instantiated I2C object 
        '''
        @brief      Initializes the I2C connection with the IMU
        '''
    
        self._I2C = I2C
        self._data2 = bytearray([0,0])
        self._data6 = bytearray([0,0,0,0,0,0])
        self._data1 = bytearray([0])
    
    def Op_mode(self, OP): #Change operating point between options described in the datasheet
        # Want IMU - ACCEL, Gyro, Relative Orientation
        '''
        @brief      Changes the operating type of the IMU
        @details    Currently the only operating types configured are for 
                    IMU and NDOF, and is used by inputting 'IMU', or 'NDOF' for OP.
        '''
        self._OP = OP
        self._I2C.mem_write(0b0000, 40, 61)
        time.sleep(0.02)
        if self._OP == 'IMU':
            self._I2C.mem_write(0b1000, 40, 61)
            time.sleep(0.01)
        if self._OP == 'NDOF':
            self._I2C.mem_write(0b1100, 40, 61)
            time.sleep(0.01)
            
    
    
     # Operating modes: --. addr = 0x3D (61)
         #[OPR_Mode]: ____
         #Configmode = 0000
         #acconly = 0001
         #magonly = 0010
         #gyroonly = 0011
         #AccMag = 0100
         #MagGyro = 0101
         #amg = 0111 - all 3 sensors
         #IMU = 1000 - High output rate (inertial measurement unit)
         #compass = 1001
         #m4g = 1010
    # Takes 7 ms to swith out of config, 19 ms to switch into config
         
    
    
    def calib(self): #Retrieve calibration status
    # Do bitwise calcs instead of list(bin(int.jjkjj))
    #ie
    #buf = i2c.mem_read(....)
    #sys = buf[0] & ob11000000
    #gyr = (buf[0]&ob00001100)>>2
    
    
        '''
        @brief Reads the calibration status and returns which sensor has been calibrated
        '''
        self._I2C.mem_read(self._data1,40, 35)
        
        
        # if (self._data1[0]&0b11000000)>>6 == 3:
        #     print('System is calibrated')
        #     # return 'sys'
        # else:
        #     print('System is not calibrated')
        #     # return None
            
        # if (self._data1[0]&0b00110000)>>4 == 3:
        #     print('Gyroscope is calibrated')
        #     # return 'gyr'
        # else:
        #     print('Gyroscope is not calibrated')
        #     # return None
            
        # if (self._data1[0]&0b00001100)>>2 == 3:
        #     print('Accelerometer is calibrated')
        #     # return 'acc'
        # else: 
        #     print('Accelerometer is not calibrated')
        #     # return None
            
        # if self._data1[0]&0b00000011 == 3:
        #     print('Magnometer is calibrated')
        #     # return 'mag'
        # else:
        #     print('Magnometer is not calibrated')
        #     # return None
        if self._data1[0]&0b11111111 == 255:
            return('calibrated')
    
    def Angles(self): #Read Euler Angles from IMU to use as State measurements - default is in degrees
    # buf - 2 bytes
    # MSB = buf[0]
    # LSB = buf[1]
    # Word = MSB <<8|LSB
    # Val = word/gain - # floating point output
    # gain - bit/degree (something like this - might be multiplied) - find the gain in the data sheet - from page 35 of the data sheet  (divide by 16)
    
    
    # Gain (degrees) - 16
    # Gain (Radians) - 900
        '''
        @brief Reads the X, Y, and Z Euler angles from the IMU
        @return Returns the Euler angles in a vector of the form [x,y,z]
        '''
        self._I2C.mem_read(self._data6,40, 26) #
        self._EX = self._data6[1]<<8|self._data6[0]
        if self._EX > 0xffff/2:
            self._EX -= 0xffff
        self._EX = self._EX/16 #The multiplier to convert to degrees
        
        self._EY = self._data6[3]<<8|self._data6[2] #Theta y
        if self._EY > 0xffff/2:
            self._EY -= 0xffff
        self._EY = self._EY/16 #The multiplier to convert to degrees
        
        self._EZ = self._data6[5]<<8|self._data6[4] # Theta X
        if self._EZ > 0xffff/2:
            self._EZ -= 0xffff
        self._EZ = self._EZ/16 #The multiplier to convert to degrees
        
        self.Euler_vector = [self._EX, self._EY,self._EZ] # X, Y, Z angles 
        return self.Euler_vector
        # use Euler x,y,z
    
    def Omega(self): #Read angular velocity from IMU to use as state measurements
        '''
        @brief Reads the X, Y, and Z angular velocity from the IMU
        @return Returns the angular velocity in a vector of the form [x,y,z]
        '''
        self._I2C.mem_read(self._data6,40, 20) #G Theta X _d
        self._GX = self._data6[1]<<8|self._data6[0]
        if self._GX > 0xffff/2:
            self._GX -= 0xffff
        self._GX = self._GX/16 #The multiplier to convert to degrees
        
        self._GY = self._data6[3]<<8|self._data6[2] #Theta Y_d
        if self._GY > 0xffff/2:
            self._GY -= 0xffff
        self._GY = self._GY/16 #The multiplier to convert to degrees
        
        self._GZ = self._data6[5]<<8|self._data6[4]
        if self._GZ > 0xffff/2:
            self._GZ -= 0xffff
        self._GZ = self._GZ/16 #The multiplier to convert to degrees
        
        self.Omega_vector = [self._GX, self._GY,self._GZ] # Z, Y, X angles
        return self.Omega_vector
        # Angles are in order Z,Y,X to correlate with the Euler Angles