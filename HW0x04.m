%% ME 405 HW0x04
% Freeman, Daniel
% Belshe, Craig

%% Define parameters
rb = 0.0105; % Radius of ball in m
rm = 0.060; % Radius of lever arm in m
lr = 0.050; % Length of push rod in m
rg = 0.042; % Vertical distance from U-joint to CG of platform in m
lp = 0.110; % Horizontal distance from U-joint to push rod pivot in m
rp = 0.0325; % Vertical distance from U-joint to push rod pivot in m
rc = 0.050; % Vertical distance from U-joint to platform surface in m
mb = 0.030; % Mass of ball in kilograms
mp = 0.400; % Mass of platform in kilograms
ip = 1.88E-3; % Moment of inertia of platform about horizontal axis through CG in kilograms*m^2
bv = 0.010; % Viscous friction at U-joint in Newton meter seconds per radian
ib = (2/5)*mb*rb^2; % Moment of inertia of ball in kilograms*m^2
g = 9.81; % Gravitational acceleration in m/s^2

%% Define equations of motion 
syms  x xd theta thetad tx
% Top left corner of M matrix
a = -(mb*rb^2 + mb*rc*rb +ib)/rb;
% Top right corner of M matrix
b = -(ib*rb + ip*rb + mb*rb^3 + mb*rb*rc^2 + 2*mb*rb^2 * rc+mp * rb*rg^2 + mb*rb*x^2)/rb;
% Bottom left corner of M matrix
c = -(mb*rb^2 + ib)/rb;
% Bottom right corner of M matrix
d = -(mb*rb^3 + mb*rc*rb^2 + ib*rb)/rb;
% Top of F vector
e = bv*thetad - g*mb*(sin(theta)*(rb+rc)+x*cos(theta))+tx*lp/rm +2*mb*thetad*x*xd-g*mp*rg*sin(theta);
% Bottom of F vector 
h = -mb*rb*x*thetad^2-g*mb*rb*sin(theta);
% M matrix
M = [a, b; c, d];
% f vector
f = [e; h];

%% Find g vector, which is [xddot; thetaddot]
q_dd = inv(M)*f;

%% Represent in standard state space form
ss = [x; theta; xd; thetad]; % State vector
ss_dot = [xd; thetad; q_dd(1); q_dd(2)]; % First derivative of state vector

%% Perform Jacobian on input and inital condition vectors
jx = jacobian(ss_dot,ss); % Jacobian of derivative of state space vector w.r.t. state space vector
u = [tx]; % Input vector
ju = jacobian(ss_dot, u); % Jacobian on derivative of state space vector w.r.t. input vector

%% Find A and B matricies
A = double(subs(jx, [x xd theta thetad], [0 0 0 0])); % substitue initial state space vector conditions
B = double(subs(ju, [x xd theta thetad], [0 0 0 0])); % substitue initial state space vector conditions
ss_dot = A*ss + B*u; % Linear state space model 

%% Plots

% Open loop, x = 0 
figure(1)
subplot(2,2,1);
plot(out.tout,out.simout(:,1)) % Position
xlabel('Time, seconds')
ylabel('Ball position, meters')
title('Ball position vs time')
subplot(2,2,2);
plot(out.tout,out.simout(:,2)) % Velocity
xlabel('Time, seconds')
ylabel('Ball velocity, m/s')
title('Ball velocity vs time')
subplot(2,2,3);
plot(out.tout,out.simout(:,3)) % Angle
xlabel('Time, seconds')
ylabel('Plate angle, radians')
title('Plate angle vs time')
subplot(2,2,4);
plot(out.tout,out.simout(:,4)) % Angular velocity
xlabel('Time, seconds')
ylabel('Plate angular velocity, rad/s')
title('Plate angular velocity  vs time')

% Open loop, x = 0.05 
figure(2)
subplot(2,2,1);
plot(out.tout,out.simout1(:,1)) % Position
xlabel('Time, seconds')
ylabel('Ball position, meters')
title('Ball position vs time')
subplot(2,2,2);
plot(out.tout,out.simout1(:,2)) % Velocity
xlabel('Time, seconds')
ylabel('Ball velocity, m/s')
title('Ball velocity vs time')
subplot(2,2,3);
plot(out.tout,out.simout1(:,3)) % Angle
xlabel('Time, seconds')
ylabel('Plate angle, radians')
title('Plate angle vs time')
subplot(2,2,4);
plot(out.tout,out.simout1(:,4)) % Angular velocity
xlabel('Time, seconds')
ylabel('Plate angular velocity, rad/s')
title('Plate angular velocity  vs time')

% closed loop, x = 0.05 
figure(1)
subplot(2,2,1);
plot(out.tout,out.simout2(:,1)) % Position
xlabel('Time, seconds')
ylabel('Ball position, meters')
title('Ball position vs time')
subplot(2,2,2);
plot(out.tout,out.simout2(:,2)) % Velocity
xlabel('Time, seconds')
ylabel('Ball velocity, m/s')
title('Ball velocity vs time')
subplot(2,2,3);
plot(out.tout,out.simout2(:,3)) % Angle
xlabel('Time, seconds')
ylabel('Plate angle, radians')
title('Plate angle vs time')
subplot(2,2,4);
plot(out.tout,out.simout2(:,4)) % Angular velocity
xlabel('Time, seconds')
ylabel('Plate angular velocity, rad/s')
title('Plate angular velocity  vs time')
