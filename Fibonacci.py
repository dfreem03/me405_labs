'''@file Fibonacci.py
The source code can be found at 
 https://bitbucket.org/dfreem03/me405_labs/src/master/Lab1/
@author Daniel Freeman
@date 1/20/2021
'''

def fib (idx):
   ''' @brief Computes Fibonacci number at index
       @details Uses a while loop to take all positive integers 
       indexes and will find the corresponding number in the
       Fibonacci sequence.
       @param idx desired index at which to find the corresponding Fibonacci number
       @return the Fibonacci number at the index
   '''
   if idx < 0:
         print ('Integer must be positive')
   elif idx == 0:
         return 0
   elif idx == 1:
         return 1
   elif idx > 1:
        A = 1
        B = 0
        n = 0
        while n < idx:
            x = A + B
            A = B 
            B = x
            n = n + 1
        return x
while True: 
    try:
        ## idx
        idx = input ('Enter the index for the Fibonacci number')
        if idx.isdigit() == True :
            idx = int(idx)
            
            print('Fibonacci number at {:} is {:}.'.format(idx,fib(idx)))
        else:
            print('Must be an integer')
    except KeyboardInterrupt:
        print("Thanks!")
        break
